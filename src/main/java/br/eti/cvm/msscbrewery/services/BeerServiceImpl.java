package br.eti.cvm.msscbrewery.services;

import java.util.UUID;

import org.springframework.stereotype.Service;

import br.eti.cvm.msscbrewery.web.model.BeerDto;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BeerServiceImpl implements BeerService {
	@Override
	public BeerDto getBeerById(UUID beerId) {
		return BeerDto.builder()
			.id(beerId)
			.beerName("Galaxy Cat")
			.beerStyle("Pale Ale")
			.build();
	}

	@Override
	public BeerDto saveNewBeer(BeerDto beerDto) {
		return BeerDto
			.builder().
			id(UUID.randomUUID())
			.build();
	}

	@Override
	public void updateBeer(UUID beerId, BeerDto beerDto) {
		log.debug("Updating a beer...");
		
	}

	@Override
	public void deleteById(UUID beerId) {
		log.debug("Deleting a beer...");
		
	}
}
