package br.eti.cvm.msscbrewery.services;

import java.util.UUID;

import br.eti.cvm.msscbrewery.web.model.BeerDto;

public interface BeerService {
	BeerDto getBeerById(UUID beerId);

	BeerDto saveNewBeer(BeerDto beerDto);

	void updateBeer(UUID beerId, BeerDto beerDto);

	void deleteById(UUID beerId);	
}
